<?php
interface ISQLHelper {
	function open();
	function doSQL($sql);
	function close();
	function get_Error();
	function getSQLErrors();
}
?>
