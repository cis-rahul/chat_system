<?php

require_once 'ICommand.php';

class CheckStatusCommand implements ICommand {

	function __construct() {
  }


	public function execute() {
		$response=new Response(Response::OK);
		$data = new stdClass;
		$loggedin=false;
		if(isset( $_SESSION['user'] ) ) {
			$user=$_SESSION['user'];		// recover the user
			$loggedin=true;
			$response->username=$user->getUsername();
			$response->name=$user->getForename()." ".$user->getSurname();
		}
		$response->loggedin=$loggedin;
		//$response->setData($data);		// This is the extra data for each command
		return($response);
	}
}


?>
