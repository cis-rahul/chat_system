<?php

require_once 'ICommand.php';

class LogoutCommand implements ICommand {
	public function LogoutCommand() {
	}
	
	
	public function execute() {
		$response=new Response(Response::OK);
		if(isset( $_SESSION['user'] ) ) {
		    unset($_SESSION['user']);
    	}
		return($response);
	}
}


?>


