<?php

require_once 'ICommand.php';

class AddContactCommand implements ICommand {

  private $email;

	function __construct() {
    }


	public function execute() {
		$response=new Response(Response::OK);
		$data = new stdClass;
		$loggedin=false;
		if(isset( $_SESSION['user'] ) ) {
			$user=$_SESSION['user'];		// recover the user
      $contactList = new ContactList($user);
      $this->email=getParameter('email');
      $contact= new User();
      $contact->loadUserFromEmail();
      // check if user is valid email here
      if (!$contact->valid) {
        # code...
        $response=new Response(Response::USER_NOT_FOUND);
        return($response);
      }
      $contactList.addContact($contact);
		} else{
      $response=new Response(Response::NOT_LOGGED_IN);
    }
		$response->loggedin=$loggedin;
		//$response->setData($data);		// This is the extra data for each command
		return($response);
	}
}


?>
