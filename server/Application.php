<?php

require_once 'Singleton.php';


class Application extends Singleton {
	private $started=FALSE;

function addColumn($tablename,$fieldname,$description,$helper) {
	$helper->doSQL("SHOW COLUMNS FROM ".$tablename." LIKE '".$fieldname."'");

	if ($helper->num_rows()==0) {
		$helper->doSQL("ALTER TABLE  ".$tablename." ADD COLUMN  ".$fieldname."  ".$description);
	}
}

function tableExists($tablename,$helper) {
	$helper->doSQL("SHOW TABLES LIKE '".$tablename."'");
	return($helper->num_rows()!=0);
}

function dropDatabaseTables() {
    $_SESSION['started'] =false;	// marked as not started
	$helper=SQLHelperManager::getHelper();
	if ($this->tableExists("Users",$helper)) {
     	$helper->doSQL("drop table Users");	// try and drop users
    }
    if ($this->tableExists("Address",$helper)) {
     	$helper->doSQL("drop table Address");	// try and drop addresses
    }
}

function checkDatabaseTables() {


     $helper=SQLHelperManager::getHelper();

		 // users table for new users on the system
     if (!$this->tableExists("Users",$helper)) {
        $sql="CREATE TABLE if not exists Users (ID int NOT NULL AUTO_INCREMENT, PRIMARY KEY (ID));";
        $helper->doSQL($sql);	// try and create user table
        $this->addColumn("Users","full_Name","VARCHAR(50)",$helper);
        $this->addColumn("Users","username","VARCHAR(50)",$helper);
        $this->addColumn("Users","email_Address","VARCHAR(100)",$helper);
        $this->addColumn("Users","password","VARCHAR(50)",$helper);
				$this->addColumn("Users","display_Status","varchar(50)",$helper);
     }


     // Contacts  table for users to add contacts in their phone book
     if (!$this->tableExists("Contacts",$helper)) {
        $sql="CREATE TABLE if not exists Contacts (ID int NOT NULL AUTO_INCREMENT, PRIMARY KEY (ID));";
        $helper->doSQL($sql);	// try and create user table
        $this->addColumn("Contacts","userID","int",$helper);	// owner of contacts
				$this->addColumn("Contacts","contactID","int",$helper);		// user id of  conatcts
     }

		// Message table for messages for chat session
	 if (!$this->tableExists("Messages",$helper)) {
				$sql="CREATE TABLE if not exists Messages (ID int NOT NULL AUTO_INCREMENT, PRIMARY KEY (ID));";
				$helper->doSQL($sql);	// try and create user table
				$this->addColumn("Messages","senderID","int",$helper);
				$this->addColumn("Messages","receiverID","int",$helper);
				$this->addColumn("Messages","sent_On","time",$helper);
				$this->addColumn("Messages","content","text",$helper);
				$this->addColumn("Messages","message_ID","int",$helper);
				$this->addColumn("Messages","chatSessionID","int",$helper);
	 }

		 // Chat Session table
		 if (!$this->tableExists("Chat_Session",$helper)) {
				$sql="CREATE TABLE if not exists Chat_Session (ID int NOT NULL AUTO_INCREMENT, PRIMARY KEY (ID));";
				$helper->doSQL($sql);	// try and create user table
				$this->addColumn("Chat_Session","chat_ID","int",$helper);		// foreign key for user of Chat Session
				$this->addColumn("Chat_Session","date_Started","date",$helper);
				$this->addColumn("Chat_Session","userID","int",$helper); // user id of user starting chat session
	}

	// Session Link Table table
	if (!$this->tableExists("UserSessionLink",$helper)) {
		 $sql="CREATE TABLE if not exists UserSessionLink (ID int NOT NULL AUTO_INCREMENT, PRIMARY KEY (ID));";
		 $helper->doSQL($sql);	// try and create user table
		 $this->addColumn("UserSessionLink","userID","int",$helper);		// user id who starts the session
		 $this->addColumn("UserSessionLink","date_Started","date",$helper);
		 $this->addColumn("UserSessionLink","sessionID","int",$helper); // session id for users to send messages to
}
     $helper->close();		// close connection
}

/**
 * @var Singleton The reference to *Singleton* instance of this class
 */

    private static $instance;

    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return Singleton The *Singleton* instance.
     */

    public static function getInstance()
    {
        if (null === Application::$instance) {
            Application::$instance = new Application();
        }

        return Application::$instance;
    }


	function start() {
		if(isset( $_SESSION['started'] ) ) {
	        $this->started=$_SESSION['started'];
	    }
		if(isset( $_SESSION['timeout'] ) ) {
	       if ($_SESSION['timeout'] + 2 * 60 < time()) {		// run this code every 2 minutes

	    	   $this->started=false;
	       }
	    }

	    if (!$this->started) {
		   $this->checkDatabaseTables();
		   $this->started=true;
		   $_SESSION['started'] =$this->started;	// marked as started
		   $_SESSION['timeout'] = time();		// used to monitor timing of session
		}
	}
}


?>
