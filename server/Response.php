<?php


include 'common.php';

class Response {

	const OK = 1;
	const AUTH_FAIL = 2;
	const NO_SUCH_COMMAND = 3;
	const USER_EXISTS = 4;
	const MISSING_ARGS = 5;
	const FATAL = 6;
	const EXCEPTION = 7;
	const NOT_LOGGED_IN = 8;
	const USER_NOT_FOUND = 9;
	const MSG_NOT_SENT = 10;






	private $messages =array('','Ok','Bad user name or password','Command not recognised',
								"Username or email already registered, pick another.","Missing or invalid arguments ",
								"Fatal","Exception", "Must be logged in to exceucte command", "User not found in DB", "Message not sent");

	// These must be public to allow for serialization

	public	$code=Response::OK;
	public  $message;
	public $sqlErrors="";
	public function setData($data) {
	    $this->data=$data;
	}

	public function __construct($code,$message=null) {
    	$this->code=$code;

		if ($message==null) {

			$this->message=$this->messages[$code];

		} else {
			$this->message=$this->messages[$code].$message;	//.. add extra info to message
		}
	}




}
?>
