<?php



function getBestSupportedMimeType($mimeTypes = null,$defaultMime) {
    // Values will be stored in this array
    $AcceptTypes = Array ();
    $accept = $_SERVER['HTTP_ACCEPT'];
    $accept = explode(',', $accept);
    foreach ($accept as $a) {
        // the default quality is 1.
        $q = 1;
        // check if there is a different quality
        if (strpos($a, ';q=')) {
            // divide "mime/type;q=X" into two parts: "mime/type" i "X"
            list($a, $q) = explode(';q=', $a);
        }
        // mime-type $a is accepted with the quality $q
        // WARNING: $q == 0 means, that mime-type isn�t supported!
        $AcceptTypes[$a] = $q;
    }
    arsort($AcceptTypes);

    // if no parameter was passed, just return parsed data
    if (!$mimeTypes) return $AcceptTypes;

    $mimeTypes = array_map('strtolower', (array)$mimeTypes);

    // let�s check our supported types:
    foreach ($AcceptTypes as $mime => $q) {
       if ($q && in_array($mime, $mimeTypes)) return $mime;
    }
    // no mime-type found
    return defaultMime;
}


function getParameter($name) {
	if (isset($_GET[$name])) {
  		return($_GET[$name]);
	}
	else {
		if (isset($_POST[$name])) {
  			return($_POST[$name]);
  		} else {
  			return("");
  		}
	}
}

// Now some SQL string builders

function makeInsertSQL($tablename,$names,$values) {
	
	$sql="insert into ".$tablename." (";
	for ($idx=0;$idx<sizeof($names);$idx++) {
		if ($idx==sizeof($names)-1) {
			$sql.=$names[$idx];			
		} else {
			$sql.=$names[$idx].",";					
		}
	}
	$sql.=") values(";
	for ($idx=0;$idx<sizeof($values);$idx++) {
		if ($idx==sizeof($values)-1) {
			$sql.="'".$values[$idx]."'";			
		} else {
			$sql.="'".$values[$idx]."'".",";					
		}
	}
	$sql.=");";
	return($sql);
}

function trace($message) {
      $echo = true;
      if ($echo)
      echo $message . "<br>";
}

function xml_encode($mixed,$rootName=null, $domElement=null, $DOMDocument=null) {
	if (is_object($mixed)) {
    	$mixed = get_object_vars($mixed);
	}
    if (is_null($DOMDocument)) {
        $DOMDocument =new DOMDocument;
        $DOMDocument->formatOutput = true;
        if ($rootName!=null) {
           $node = $DOMDocument->createElement($rootName);
           $DOMDocument->appendChild($node);
     	   xml_encode($mixed, null,$node, $DOMDocument);
           return($DOMDocument->saveXML());
        }
        xml_encode($mixed, null,$DOMDocument, $DOMDocument);
        return($DOMDocument->saveXML());
    }
    else {
        if (is_array($mixed)) {
            foreach ($mixed as $index => $mixedElement) {
                if (is_int($index)) {
                    if ($index === 0) {
                        $node = $domElement;
                    }
                    else {
                        $node = $DOMDocument->createElement($domElement->tagName);
                        $domElement->parentNode->appendChild($node);
                    }
                }
                else {
                    $plural = $DOMDocument->createElement($index);
                    $domElement->appendChild($plural);
                    $node = $plural;
                    if (!(rtrim($index, 's') === $index)) {
                        $singular = $DOMDocument->createElement(rtrim($index, 's'));
                        $plural->appendChild($singular);
                        $node = $singular;
                    }
                }
 
                xml_encode($mixedElement, null,$node, $DOMDocument);
            }
        }
        else {
            $domElement->appendChild($DOMDocument->createTextNode($mixed));
        }
    }
}

$json=false;
$debugMode=FALSE;

function encodeResponse($response) {
	global $debugMode;
    
	if (!$debugMode) {
		ob_clean();		// nice clean response if not in debug mode
	}
	global $json;
	if ($json) {
		return(json_encode($response));
	} else {
		header("Content-Type: application/xml");
		$encoded=xml_encode($response,"Response");
		return($encoded);
	}
}

function exceptionToString($exception) {
	global $debugMode;
	$stack=$exception->getTrace();
	if ($debugMode) {
	$html="<br/><br/>".$exception->getMessage()."<br/><br/>".$exception->getFile()." on line ".$exception->getLine()."<br/><br/>";  
	for ($idx=0;$idx<sizeof($stack);$idx++) {
		$dump=$stack[$idx];
		$html.=$dump['file']." in function ".$dump['function']." at line ".$dump['line']."<br/><br/>";
	}
	return($html);
	} else {
		// Exception handling for JSON interface
		$response = new stdClass;
     	$response->code=Response::EXCEPTION;
     	$stackData=array();
    	for ($idx=0;$idx<sizeof($stack);$idx++) {
			$dump=$stack[$idx];
			$stackData[$idx]=$dump['file']." in function ".$dump['function']." at line ".$dump['line'];
		}	 
		$response->stack=$stackData;
     	$response->message="Exception";
		return(encodeResponse($response));	
	}
}


function setMimeType() {
    global $debugMode;
    global $json;
	$mtype=getBestSupportedMimeType( array('application/json','application/xml', 'text/html'), 'text/html');
	header('Content-Type: '.$mtype);
	
	if (strcmp($mtype,'application/xml')==0) {
		$json=false;	// default is json, switch of json for XML output
	} else {
		$json=true;
	}
	
	if (strcmp($mtype,'text/html')==0) {
		$debugMode=true;
		error_reporting(E_ALL);
	} else {
		$debugMode=false;
		error_reporting(0);	// Application does its own reporting when running in Ajax mode	
	}
		
}

?>
