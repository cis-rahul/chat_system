<?php


include 'common.php';

class User {
	private $id="";
	// User properties stored in database
	private $fullname="";
	private $username="";
	private $email="";
	private $password="";

	private $debug=FALSE;

	private $error="";
	private $valid=FALSE;

	public function isValid() {
		return($this->valid);
	}

	public function getID() {
		return($this->id);
	}

	public function get_error() {
		return($this->error);
	}

	public function getUserName() {
		return($this->username);
	}

	public function setUserName($name) {
		$this->username=$name;
	}

	public function getForename() {
		return(ucfirst($this->forename));
	}

	public function getSurname() {
		return($this->surname);
	}

	// Loads fields into database
	private function loadFields($row) {
			$this->id=$row['ID'];
			$this->fullname=$row['full_Name'];
			$this->username=$row['username'];
			$this->email=$row['email_Address'];
			$this->password=$row['password'];
	}

public function loadUserFromID($id) {
		$sql="select * from Users where ID='" . $id . "'";
		$helper=SQLHelperManager::getHelper();
			$results=$helper->doSQL($sql);	// try and create user table

		$this->valid=FALSE;
		if ($helper->num_rows()==0){
							return;
				}
		// could not find  user account
		if ($row = $helper->fetch_row()) {		// look for row
						$this->loadFields($row);
			$this->valid=TRUE;
		}
	}


	public function loadUserFromEmail($email) {
		$sql="select * from Users where email_Address='" . $email . "'";
           if ($this->debug) {
                 echo "Trying to load up user :".$email;
           }
		$helper=SQLHelperManager::getHelper();
     	$results=$helper->doSQL($sql);	// try and create user table

		$this->valid=FALSE;
		if ($helper->num_rows()==0) {
              if ($this->debug) {
                  echo "Row count 0 could not find user :";
              }
              return;
        }
    // could not find account
		if ($row = $helper->fetch_row()) {		// look for row
		    if ($this->debug) {
                  echo "loading row!";
            }
            $this->loadFields($row);
			$this->valid=TRUE;
            if ($this->debug) {
               echo "Loaded up user ok! :".$this->password;
            }
		} else {
		    if ($this->debug) {
            		 echo "Could not load user row is NULL";
            }
		}

	}

	public function saveUser() {
		$sql="update Users set";
		$sql.=" full_Namename='".$this->fullname."',";
		$sql.=" username='".$this->username."',";
		$sql.=" email_Address='".$this->email."',";
		$sql.=" password='".$this->password."',";

		// ADD more fields here, use template above this line

		$sql.=" mobile='".$this->mobile."' ";

		$sql.=" where id='".$this->id."' ";
		$helper=SQLHelperManager::getHelper();
     	$helper->doSQL($sql);	// save
	}


	public static function userExists($email) {
		$sql="select * from Users where email_Address='" . $email . "'";
		$helper=SQLHelperManager::getHelper();
     	$helper->doSQL($sql);	// try and create user table
		return($helper->num_rows()!=0);
	}




	public static function addUser($fullname,$username,$email,$password) {
		$password=md5($password);
		$sql=makeInsertSQL("Users",array("ID","full_Name","username","email_Address","password"),array(0,$fullname,$username,$email,$password));
		$helper=SQLHelperManager::getHelper();
	    $helper->doSQL($sql);	// try and create user table
	    if ($helper->get_Error()) {
	    	$helper->close();
	    	return(FALSE);			// failed to insert
	    }
	    $helper->close();
	    return(TRUE);
	}


	public static function checkPassword($email,$password) {
		$loadedUser=new User();	// create user
		$loadedUser->loadUserFromEmail($email);
		$password=md5($password);
		if (!$loadedUser->valid) {
			$loadedUser->error="Bad username or password  INVALID";
			return($loadedUser);
		}
		if ($loadedUser->password!=$password) {
		      $loadedUser->error="Bad username or password";
		      $loadedUser->valid=FALSE;
		      return($loadedUser);
		}
		return($loadedUser);
	}
}
