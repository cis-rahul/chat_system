<?php

class Validator {

	private $errorMessage="";
	private $valid=FALSE;
	
	
	public function getErrorMessage() {
		return($this->errorMessage);
	}

	// Valdation Method for new user, sets error message if user is not valid
	public function loginDetailsValid($email,$password) {
		$this->errorMessage="";
		$error=FALSE;
		if (strlen($email)<1) {
			$this->errorMessage=" email";
			$error=true;
		}
		if (strlen($password)<1) {
			$this->errorMessage.=" password";
			$error=true;			
		}
		$this->errorMessage.=" is missing";
		return(!$error);		
	}
	

	// Valdation Method for new user, sets error message if user is not valid
	public function newUserValid($fullname,$username,$email,$password) {
		$this->errorMessage="";
		if (strlen($username)<1) {
			$this->errorMessage=" username is missing";
			return(FALSE);
		}
		if (strlen($fullname)<1) {
			$this->errorMessage=" fullname is missing";
			return(FALSE);
		}
		if (strlen($password)<8) {
			$this->errorMessage=" password must be at least 8 characters";
			return(FALSE);
		}
		if (strlen($email)<4) {
			$this->errorMessage=" email missing";
			return(FALSE);
		}
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->errorMessage="Error, please enter a valid email address";
			return(FALSE);
		}
		return(TRUE);
	}
}

?>
