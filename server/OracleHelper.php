<?php
require_once 'ISQLHelper.php';
class OracleHelper implements ISQLHelper {
	private $username = "";
	private $password = "";
	private $host = "";
	private $database = "";
	private $stid = null;
	private $con = null;
	private $row=null;
	private $results;
	private static $sqlErrors="";
	
	// private $debug = TRUE;
	private $error = FALSE;
	private $debug = FALSE;
	private $sqlDebug = FALSE;
	private $sql = "";
	
	public function getSQLErrors() {
		return($sqlErrors);
	}
	
	
	function OracleHelper($host, $username, $password, $database) {
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->database = $database;
	}
	
	// private $sqlDebug=FALSE;
	function open() {
		if ($this->con == null) {
			$this->con = oci_connect ( $this->username, $this->password, $this->host);
				
		}
		
		if (! $this->con) {
				$e = oci_error ();
				throw new Exception("Failed to connect to Oracle: " . $e ['message']);
		} else {
			if ($this->debug) {
				echo "Connected ok";
			}
		}
	}
	function doSQL($sql) {
		$this->sql = $sql;
		$this->error = FALSE;
		if ($this->sqlDebug) {
			echo "Trying to execute ... " . $sql."<br/>";
		}
		$this->open ();
		if (!$this->con) {
			return;
		}
		if (!$this->stid = oci_parse ( $this->con, $sql )) {
			return(FALSE);
		}
		$ok = oci_execute ($this->stid);
		if (! $ok) {
			$this->error = TRUE;
			if ($this->debug) {
				$e = oci_error ();
			}
			return ($ok);
		} else {
		    $this->row=oci_fetch_array ( $this->stid );
		}
	}
	function fetch_row() {
		return ($this->row);
	}
	function close() {
		oci_close ( $this->con );
	}
	function get_Error() {
		return ($this->error);
	}
	
	function num_rows() {
		return(oci_num_rows($this->stid));
		
	}
	
}
?>
