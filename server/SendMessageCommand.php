<?php
include 'common.php';

# Message Class

class SendMessageCommand implements ICommand {

  private $receiverID;
  private $message;

  public function SendMessageCommand() {
		$this->receiverID=getParameter('ID');
    $this->message=getParameter('content');
  }

  public function execute() {
    $response=new Response(Response::OK);

    if(!isset($_SESSION['user'])){
      $response=new Response(Response::NOT_LOGGED_IN);
      return($response);
    }


    $loadedUser=new User();
    $loadedUser->loadUserFromID($this->receiverID);
    if (!$loadedUser->isValid()) {
      $response=new Response(Response::USER_NOT_FOUND);
      return($response);
		}

    $user=$_SESSION['user'];
    $userID=$user->getID();
    #sql insert statments
    $sql=makeInsertSQL("Messages",array("ID","senderID","receiverID","content"),
                        array(0,$userID,$this->receiverID,$this->message));

    $helper=SQLHelperManager::getHelper();
    $helper->doSQl($sql);



    return($response);
  }

}
?>
