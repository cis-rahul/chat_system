<?php



try {


include 'common.php';


// General command execution module
// First start global session for all commands
session_start();

// First set the mime type depending on context
setMimeType();



// Now check if the database tables etc are set up ok
Application::getInstance()->start();	// start up application

// We now getting the command name and switch on it
$commandName=getParameter('command');
// Default response set first
$response=new Response(Response::NO_SUCH_COMMAND);
if (strlen($commandName)==0) {
	$response=new Response(Response::MISSING_ARGS," command name missing add in example: ?command=login");
}

$command=null;

switch ($commandName) {
	case "login" :
		$command=new LoginCommand();
		break;
	case "logout" :
		$command=new LogoutCommand();
		break;
	case "register" :
		$command=new RegisterCommand();
		break;
	case "status" :
		$command=new CheckStatusCommand();
		break;
	case "addcontact" :
		$command = new AddContactCommand();
		break;
	case "sendMessage" :
		$command = new SendMessageCommand();
		break;
	case "getMessages" :
		$command = new GetMessagesCommand();
		break;
	default :
		break;

}
if ($command!=null) {
		$response=$command->execute();
		//$response->setSqlErrors(SQLHelperManager::getSQLErrors());
}
echo encodeResponse($response);

} catch (Exception $e) {
      echo exceptionToString($e);
}
?>
