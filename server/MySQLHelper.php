<?php

require_once 'ISQLHelper.php';



class MySQLHelper implements ISQLHelper {
	private $username = "root";
	private $password = "root";
	private $host = "localhost";
	private $database = "Chat_Application_Final";
	public static $sqlErrors='';	
	private $con = null;
	private $results;
	
	private $debug = FALSE;
	private $error = FALSE;
	//private $debug=FALSE;
	private $sqlDebug=TRUE;
	
	public function getSQLErrors() {
		return(MySQLHelper::$sqlErrors);
	}
	
	
	function __construct($host, $username, $password, $database) {
    	$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->database = $database;
	}
	
	
	function open() {
		if ($this->con == null) {
			
			$this->con = mysqli_connect ($this->host,$this->username,$this->password,$this->database );
		}
		
		if (mysqli_connect_errno ()) {
			throw new Exception("Failed to connect to MySQL: " . mysqli_connect_error ());
		} else {
			if ($this->debug) {
				
				echo "Connected ok";
			}
		}
	}
	function doSQL($sql) {		
		$this->error = FALSE;
		$this->open ();
		if ($this->results = mysqli_query ( $this->con, $sql )) {
			if ($this->debug) {
			   echo "SQL Executed OK";
			}
		} else {
			$this->error = TRUE;
			MySQLHelper::$sqlErrors.="SQL is ".'$sql'." response is ".mysqli_error ( $this->con );
			if ($this->sqlDebug) {
				throw new Exception("Error executing  ".$sql." MYSQL responded ".mysqli_error ( $this->con ));
			}
		}
		return ($this->results);
	}
	
	
	function close() {
		if ($this->con!=null) {
			mysqli_close ($this->con);
		}
	}
	function get_Error() {
		return ($this->error);
	}
	
	function fetch_row() {
		if ($this->results==null) {
			echo "No results";
		}
		$row=mysqli_fetch_array($this->results);
		if (!$row) {
			echo "No row!!!";
		}
	    return($row);	    
	}
	
	function num_rows() {
		return($this->results->num_rows);
	}

	function insert($query){
		$this->open ();
		$data = mysqli_query ($this->con, $query) ;
		if($data){
			return 1;
		}
		else{
			return 0;
		}

	}
}

?>
