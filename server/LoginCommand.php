<?php

require_once 'ICommand.php';

class LoginCommand implements ICommand {

	private $email;
	private $password;

	public function LoginCommand() {
		$this->email=getParameter('email');
		$this->password=getParameter('password');
	}

	private function doLogin() {
		// Do execution of login command
		// First check if arguments are valid
		$validator=new Validator();

		if (!$validator->loginDetailsValid($this->email,$this->password)) {
			// Arguments not passed
			$response=new Response(Response::MISSING_ARGS,$validator->getErrorMessage());
			return($response);
		}

		// Now see if this user is on dbase and password checks out
		$user=User::checkPassword($this->email,$this->password);

		if ($user->isValid()) {
			$response=new Response(Response::OK);
			$data = new stdClass;
			$_SESSION['user']=$user;	// save the user
			$data->welcome="Welcome to this application ". $user->getForename();
			$response->setData($data);
		} else {
			$response=new Response(Response::AUTH_FAIL);
		}

		return($response);
	}

	public function execute() {
		$response=$this->doLogin();
		return($response);
	}
}


?>
