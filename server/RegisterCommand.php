<?php

require_once 'ICommand.php';

class RegisterCommand implements ICommand {

	private $fullname;
	private $username;
	private $email;
	private $password;


	public function RegisterCommand() {
		$this->fullname=getParameter('fullname');
		$this->username=getParameter('username');
		$this->email=getParameter('email');
		$this->password=getParameter('pwd');
	}

	private function doRegister() {
		$validator=new Validator();

		if (!$validator->newUserValid($this->fullname,$this->username,$this->email,$this->password)) {
			$this->response=new Response(Response::MISSING_ARGS,$validator->getErrorMessage());
			return($this->response);
		}

		if (User::userExists($this->email)) {
			$this->response=new Response(Response::USER_EXISTS);
			return($this->response);
		}

		User::addUser($this->fullname,$this->username,$this->email,$this->password);

		$response=new Response(Response::OK);

		return($response);
	}

	public function execute() {
		$response=$this->doRegister();
		return($response);
	}
}


?>
