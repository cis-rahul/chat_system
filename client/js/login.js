// Login using data from web page

function LoginHelper(buttonID,usernameID,passwordID,responseDiv,loggedInPage) {
	$("#"+buttonID).click(function(){

		$("#"+responseDiv).html("");		// clear out response before request

		// Does this need to be changed if i change field name in html file
		var email = $("#"+usernameID).val();
		var password = $("#"+passwordID).val();

		serverAPI.makeRequest(
		   function(response) {
			   // Call back
			   if (response.code==ServerAPI.OK) {
				   window.location.href=loggedInPage;
			   } else {
				   $("#"+responseDiv).html(response.message);
			   }
		   }
		   ,"login",{email:email,password:password});
	});
}


$(document).ready(function() {

	var loginHelper=new LoginHelper("loginButton","username","password","responseDiv","chatRoom.html");

});
