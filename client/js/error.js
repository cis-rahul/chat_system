


var forceDebug=true;
var noDebug=false;

function replaceAll(str, find, replace) {
	  return str.replace(new RegExp(find, 'g'), replace);
}

function showError(message) {
	message=replaceAll(message,"at ","<br/><br/> at ");
	var div = document.createElement("div");
	div.style.position="absolute";
	div.style.top = "5vh";
	div.style.left = "10vw";
	div.style.width = "80vw";
	div.style.height = "80vh";
	div.style.borderStyle="outset";
	div.style.borderColor="black";
	
	div.style.padding="2%";
	div.style.backgroundColor = "#c0c0c0";
	div.style.color = "black";
	div.innerHTML = message;
	var button = document.createElement("BUTTON");
	button.style.position="absolute";
	
	button.style.top = "80vh";
	button.style.left = "2vw";
	
	button.innerHTML = "Close";
	
	button.onclick=function() {
		div.style.display="none";
		button.style.display="none";
	}
	var win = window.open("", "Error", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top="+(100)+", left="+(100));
	win.document.body.innerHTML = div;
	
	div.appendChild(button);
	win.document.body.innerHTML = message;
	
}

function errorHandler(message,source,line,col,error) {
	
	var stack=error.stack;
	var isdebug=(window.location.href.indexOf("localhost")!=-1);
	if (!isdebug) {
		isdebug=(window.location.href.indexOf("127.0.0.1")!=-1);
	}
	if (forceDebug)  {
		isdebug=true;
	}
	if (noDebug) {
		isdebug=false;
	}
	if (!isdebug) {
		return true;
	}
	// Now build up error string
	var errorString="Got error "+message;
	errorString+=" in file "+source;
	errorString+=" at line number "+line;
	errorString+=" col is "+col+"<br/>";
	if (error.stack!=undefined) { 
		errorString+="<br/>Stack Trace:<br/><br/>"+error.stack;
	}
	
	
	showError(errorString);
}

window.onerror=errorHandler;
