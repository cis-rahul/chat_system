//  Client javascript interface
//  This method pushes commands to the servers and handles the return

function ServerAPI() {

	ServerAPI.OK=1;
	ServerAPI.AUTH_FAIL = 2;
	ServerAPI.NO_SUCH_COMMAND = 3;
	ServerAPI.USER_EXISTS = 4;
	ServerAPI.MISSING_ARGS = 5;
	ServerAPI.FATAL = 6;
	ServerAPI.EXCEPTION = 6;


	var requestURL="../server/command.php";
	var requestTimeout=30000;
	var debug=true;
	
	// Push request to remote server
	this.makeRequest = function(callback,command,arguments) {
		var requestData="command="+command;
		for (var key in arguments) {
			requestData+="&"+key+"="+arguments[key];
		}
		requestData+="&";
		if (debug) {
			alert("Request is "+requestData);
		}
		$.ajaxSetup ({
				cache: false
			});
			var request=$.ajax({
				type: "POST",
				url: requestURL,
				data: requestData,
				dataType: "text",
				cache: false,
				headers: {
			        Accept : "application/json"
			    },
				timeout: requestTimeout,
				error : function(response) {
					alert("Failed to connect to "+requestURL);
					
					var response={};
					response.code=255;
					response.message="Failed to connect, please check you Internet/Wifi connection";
					callback(response);
				},
				success: function(msg){
					var response=null;
					var errorParse=false;
					try {
						response=JSON.parse(msg);
					} catch (error) {
						errorParse=true;
						response={};		// dummy respone failed to parse
					}
					if (debug) {
						
						if ((response.code==ServerAPI.EXCEPTION) || (response.fatal) || (errorParse)) {
							var gotodebug=confirm("Error, executing PHP debug request in browser? ")
						 	if (gotodebug) {
								window.location.href=requestURL+"?"+requestData;
							}
						}	
					}
					callback(JSON.parse(msg));
					
				}
			});
		};

		this.logout=function() {
			serverAPI.makeRequest(
					function(response) {

					}
					,"logout",{});
		}

};


var serverAPI=new ServerAPI();
