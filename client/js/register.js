// Register support Javascript


function RegisterHelper(buttonID,emailID,passwordID,fullnameID,usernameID,responseDiv,registeredPage) {
	$("#"+buttonID).click(function(){

		$("#"+responseDiv).html("");		// clear out response before request

    var email = $("#"+emailID).val();
		var password = $("#"+passwordID).val();
		var fullname = $("#"+fullnameID).val();
		var username = $("#"+usernameID).val();

		serverAPI.makeRequest(
		   function(response) {
			   // Call back
			   if (response.code==ServerAPI.OK) {
				   window.location.href=registeredPage;
			   } else {
				   $("#"+responseDiv).html(response.message);
			   }
		   }
		   ,"register",{email:email,pwd:password,username:username,fullname:fullname});
	});

};





$(document).ready(function() {


	var registerHelper=new RegisterHelper("registerButton","email","pwd","fullname","username","responseDiv","login.html");




});
